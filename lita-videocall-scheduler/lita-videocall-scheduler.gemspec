Gem::Specification.new do |spec|
  spec.name          = "lita-videocall-scheduler"
  spec.version       = "0.1.0"
  spec.authors       = ["Miguel Angel Fernandez"]
  spec.description   = ""
  spec.summary       = ""
  spec.homepage      = "https://sindicatodelcodigo.es"
  spec.license       = ""
  spec.metadata      = { "lita_plugin_type" => "handler" }

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "lita", ">= 4.7"
  spec.add_runtime_dependency "rufus-scheduler", "~> 3.5.2"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rack-test"
  spec.add_development_dependency "rspec", ">= 3.0.0"
  spec.add_development_dependency "mock_redis", ">= 0.19.0"
end
