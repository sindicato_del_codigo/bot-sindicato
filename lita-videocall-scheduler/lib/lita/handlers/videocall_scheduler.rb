module Lita
  module Handlers
    class VideocallScheduler < Handler
      config :notifications_room

      route(/programa video el (\d{2}\/\d{2}\/\d{4}) a las (\d{2}:\d{2})/, :schedule)

      REMINDERS_KEY = name.to_s

      def schedule(response)
        typed_date, typed_time = response.matches.first

        unless(valid_time?(typed_time))
          response.reply(t('time-exception'))
          return
        end

        unless(valid_date?(typed_date))
          response.reply(t('date-exception'))
          return
        end

        reminder_time = to_time(typed_date, typed_time)
        store_remider(reminder_time)
        schedule_reminder(reminder_time)

        response.reply(t('success', date: typed_date, time: typed_time))
      end

      on(:loaded) do
        get_reminders.each do |reminder_time|
          schedule_reminder(reminder_time)
        end
      end

      on(:shut_down_started) do
        robot.scheduler.shutdown
      end

      private

      def to_time(date, time)
        DateTime.strptime("#{date} #{time} +02:00", '%d/%m/%Y %H:%M %z').to_time #TODO: get the timezone from the user profile
      end

      def store_remider(time)
        redis.sadd(REMINDERS_KEY, time.to_s)
      end

      def get_reminders
        redis.smembers(REMINDERS_KEY)
      end

      def get_call_instructions
        VideocallService.new(redis).get_instructions
      end

      def schedule_reminder(time)
        robot.scheduler.at(time, NotificationJob.new(robot, VideocallService.new(redis).get_instructions, config.notifications_room))
      end

      def valid_date?(date)
        Date.strptime(date, '%d/%m/%Y') && true
      rescue ArgumentError
        false
      end

      def valid_time?(time)
        Time.parse(time) && true
      rescue ArgumentError
        false
      end

      class VideocallService
        KEY_LAST_VIDEO_INDEX = 'last_video_index'
        def initialize(redis)
          @redis = redis
        end

        def get_instructions
          return '<error retrieving a call instructions>' if raw_env.nil? || raw_env.empty?
          call_position = call_position_in_urls

          @redis.set(KEY_LAST_VIDEO_INDEX, call_position)

          call_urls[call_position]
        end

        private

        def call_position_in_urls
          call_position = @redis.exists(KEY_LAST_VIDEO_INDEX) ? (@redis.get(KEY_LAST_VIDEO_INDEX).to_i + 1) % call_urls.length : 0
        end

        def call_urls
          raw_env.nil? ? '' : raw_env.split(';')
        end

        def raw_env
          ENV['CONFERENCE-SERVICE']
        end
      end

      NotificationJob = Struct.new(:robot, :video_instructions, :room) do
        def call
          target = Source.new(room: room)
          robot.send_messages(target, message)
        end

        def message
          I18n.translate('lita.handlers.videocall_scheduler.reminder', instructions: video_instructions)
        end

      end
      Lita.register_handler(self)
    end
  end
end
