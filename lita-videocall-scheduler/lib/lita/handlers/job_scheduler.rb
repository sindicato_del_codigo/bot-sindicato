require 'rufus-scheduler'

module Lita
  module ScheduleRobot
    def initialize(registry=Lita)
      @scheduler = Rufus::Scheduler.new
      super
    end

    def scheduler
      @scheduler
    end
  end

  class Robot
    prepend ScheduleRobot
  end
end
