require "lita"

Lita.load_locales Dir[File.expand_path(
  File.join("..", "..", "locales", "*.yml"), __FILE__
)]

# require "lita/handlers/videocall_scheduler"
require_relative "./lita/handlers/job_scheduler"
require_relative "./lita/handlers/videocall_scheduler"

Lita::Handlers::VideocallScheduler.template_root File.expand_path(
  File.join("..", "..", "templates"),
 __FILE__
)
