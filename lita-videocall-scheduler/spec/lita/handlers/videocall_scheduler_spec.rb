require 'spec_helper'
require 'mock_redis'

describe Lita::Handlers::VideocallScheduler, lita_handler: true do
  describe 'correct message' do
    it 'replies with a confirmation' do
      send_message('programa video el 18/06/2018 a las 21:30')

      expect(replies.last).to eq('A videocall has been scheduled at 18/06/2018 - 21:30')
    end

    it 'schedules a job at the specified time' do
      send_message('programa video el 18/06/2100 a las 21:00')

      scheduled_job_time = robot.scheduler.at_jobs.last.original.to_s
      expect(scheduled_job_time).to eq("2100-06-18 21:00:00 +0200")
    end
  end

  describe 'invalid message' do
    describe 'with invalid date' do
      it 'replies with a clue' do
        send_message('programa video el 18/13/2018 a las 21:30')

        expect(replies.last).to eq('date format must be dd/MM/yyyy')
      end
    end

    describe 'with invalid time' do
      it 'replies with a clue' do
        send_message('programa video el 18/06/2018 a las 21:63')

        expect(replies.last).to eq('time format must be hh:mm')
      end
    end
  end

  describe 'reminder triggered' do
    it 'the videocall instructions are specified in the reminder'do
      ENV['CONFERENCE-SERVICE'] = 'http://a-conference.com'

      expect(robot.scheduler).to receive(:at) do |_, job|
        expect(job.message).to eq('the videocall http://a-conference.com is about to start' )
      end

      send_message('programa video el 18/06/2100 a las 21:00')
    end

   it 'every reminder has a different videocall, in a round-robin way' do
      ENV['CONFERENCE-SERVICE'] = 'http://a-conference.com;http://another-conference.com'

      call = Lita::Handlers::VideocallScheduler::VideocallService.new(MockRedis.new)

      expect(call.get_instructions).to eq('http://a-conference.com')
      expect(call.get_instructions).to eq('http://another-conference.com')
      expect(call.get_instructions).to eq('http://a-conference.com')
    end

    it 'has a default message when no calls are configured' do
      ENV['CONFERENCE-SERVICE'] = nil

      call = Lita::Handlers::VideocallScheduler::VideocallService.new(MockRedis.new)
      expect(call.get_instructions).to eq('<error retrieving a call instructions>')

      ENV['CONFERENCE-SERVICE'] = ''

      call = Lita::Handlers::VideocallScheduler::VideocallService.new(MockRedis.new)
      expect(call.get_instructions).to eq('<error retrieving a call instructions>')
    end
  end

  describe Lita::Handlers::VideocallScheduler::NotificationJob do

    xit 'sends a message'

  end


  describe 'the robot is loaded' do
    it 'schedules all the pending jobs' do
      send_message('programa video el 18/06/2100 a las 21:00')
      expect(robot.scheduler.at_jobs.count).to eq(1)
      robot.scheduler.at_jobs.last.unschedule
      expect(robot.scheduler.at_jobs.count).to eq(0)

      robot.trigger(:loaded)

      scheduled_job_time = robot.scheduler.at_jobs.last.original.to_s
      expect(scheduled_job_time).to eq("2100-06-18 21:00:00 +0200")
    end
  end
end
