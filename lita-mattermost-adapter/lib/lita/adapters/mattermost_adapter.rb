require_relative './mattermost/rtm_connection'
require_relative './mattermost/api'

module Lita
  module Adapters

    class MattermostAdapter < Adapter

      config :robot_id, type: String
      config :websocket_url, type: String

      def mention_format(name)
        mention_prefix='@'
        mention_prefix + name.to_s
      end

      def run
        return if connection?
        build_connection
        connection.run
      end


      def send_messages(receiver, messages)
        channel= channel_for(receiver)
        send(channel,messages)
      end

      def shut_down
        return unless connection?
        connection.shut_down
        notify_closure
      end

      private

      attr_reader :connection

      def notify_closure
        robot.trigger(:disconnected)
      end

      def send(channel,messages)
        api = API.new(config)
        api.send_messages(channel, messages)
      end

      def connection?
        connection
      end

      def build_connection
        @connection = RTMConnection.build(robot, config)
      end

      def channel_for(source)
        return source.room
      end

    end

    Lita.register_adapter(:mattermost_adapter, MattermostAdapter)
  end
end
