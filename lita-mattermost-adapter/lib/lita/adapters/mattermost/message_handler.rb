module Lita
  module Adapters
    class MattermostAdapter < Adapter
      class MessageHandler
        def initialize(robot, robot_id, data)
          @robot = robot
          @robot_id = robot_id
          @type = data["event"]
          @post = Post.new(data)
        end

        def handle
          case type
          when "hello"
            handle_hello
          when "posted"
            handle_message
          else
            handle_unknown
          end
        end

        private

        attr_reader :robot
        attr_reader :robot_id
        attr_reader :type

        def dispatch_message(user)
          room = Lita::Room.find_by_id(@post.channel_id)
          source = Source.new(user: user, room: room || @post.channel_id)

          message = Message.new(robot, @post.message, source)

          log.debug("Dispatching message to Lita from #{user.id}.")
          robot.receive(message)
        end

        def from_self?(user)
          user.id == robot_id
        end

        def handle_hello
          log.info("Connected to Mattermost.")
        end

        def handle_message
          user = User.find_by_id(@post.user_id) || User.create(@post.user_id)

          return if from_self?(user)

          dispatch_message(user)
        end

        def handle_unknown
          log.debug("#{type} event received from Mattermost and will be ignored.")
        end

        def log
          Lita.logger
        end

        class Post
          def initialize(data)
            if(data["data"].nil? || data["data"]["post"].nil?)
              @post = {}
            else
              @post = MultiJson.load(data["data"]["post"])
            end
          end

          def user_id
            @post["user_id"]
          end

          def channel_id
            @post["channel_id"]
          end

          def message
            @post["message"]
          end
        end
      end
    end
  end
end
