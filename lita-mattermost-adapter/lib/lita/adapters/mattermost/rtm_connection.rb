require 'faye/websocket'
require 'multi_json'

require_relative './event_loop'
require_relative './message_handler'

module Lita
  module Adapters
    class MattermostAdapter < Adapter
      class RTMConnection
        class << self
          def build(robot, config)
            new(robot, config)
          end
        end

        def initialize(robot, config)
          @robot = robot
          @websocket_url = config.websocket_url
          @robot_id = config.robot_id
        end

        def run(queue = nil, options = {})
          EventLoop.run do
            connect(options)

            websocket.on(:open) { authenticate }
            websocket.on(:message) { |event| receive_message(event) }
            websocket.on(:close) { disconnect }
            websocket.on(:error) { |event| error(event) }

            queue << websocket if queue
          end
        end

        def shut_down
          log_shut_down
          if websocket && EventLoop.running?
            websocket.close
          end

          EventLoop.safe_stop
        end

        private

        def connect(options)
          log_connect

          @websocket = Faye::WebSocket::Client.new(
            websocket_url,
            nil,
            websocket_options.merge(options)
          )
        end

        def log_connect
          log.debug("Connecting to the Mattermost Real Time Messaging API.")
        end

        def authenticate
          log_connected

          websocket.send(auth_payload)
        end

        def auth_payload
          auth_message = {
            "seq": 1,
            "action": "authentication_challenge",
            "data": {
              "token": ENV['token']
            }
          }
          MultiJson.dump(auth_message)
        end

        def log_connected
          log.debug("Connected to the Mattermost Real Time Messaging API.")
        end

        def disconnect
          log_disconnected

          EventLoop.safe_stop
        end

        def log_disconnected
          log.info("Disconnected from Mattermost.")
        end

        def error(event)
          log.debug("WebSocket error: #{event.message}")
        end

        def log_shut_down
          log.debug("Closing connection to the Mattermost Real Time Messaging API.")
        end

        attr_reader :robot
        attr_reader :robot_id
        attr_reader :websocket
        attr_reader :websocket_url

        def log
          Lita.logger
        end

        def receive_message(event)
          data = MultiJson.load(event.data)
          log.debug(data)

          EventLoop.defer { MessageHandler.new(robot, robot_id, data).handle }
        end

        def websocket_options
          { ping: 10 }
        end
      end
    end
  end
end
