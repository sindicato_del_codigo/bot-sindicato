# Bot-Sindicato


## Installation

You would need a redis instance to run anything in your localhost.
Lita comes with a default configuration that matches the redis default

### Run a redis instance with docker
```bash
docker pull redis
docker run --name redis -p 6379:6379  redis:latest
```

### Configuration
The bot needs the following environment variables:

* `townsquare`: the room id where the videocall notifications are posted
* `token`: authentication token from a mattermost login request
* `CONFERENCE-SERVICE`: list of urls that will be assigned to each scheduled event

